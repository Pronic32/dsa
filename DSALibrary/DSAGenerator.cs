﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace DSALibrary
{
  public class DSAGenerator
  {
    public readonly int N;
    public readonly int L;

    private Random _random;

    private BigInteger _q;
    private BigInteger _p;
    private BigInteger _h;
    private BigInteger _g;

    public DSAGenerator()
    {
      N = 256;
      L = 2048;
      _random = new Random();

      GenerateParameters();
    }

    private void GenerateParameters()
    {
      _q = GenerateNumber(N);
      _p = GenerateNumberByDivisor(L, _q);

      _h = 2;
      _g = BigInteger.ModPow(_h, BigInteger.Divide(_p - 1, _q), _p);
    }

    /// <summary>
    /// Generate prime number
    /// </summary>
    /// <param name="parSize">Size in bits</param>
    /// <returns></returns>
    private BigInteger GenerateNumber(int parSize)
    {
      string primeNumber = String.Empty;
      for (int i = 0; i < N / 8; i++)
      {
        primeNumber += _random.Next(0, 9);
      }

      return BigInteger.Parse(primeNumber);
    }

    /// <summary>
    /// Generate prime propopotional to divisor - 1 number
    /// </summary>
    /// <param name="parSize">Size in bits</param>
    /// <param name="parDivisor">Divisor</param>
    /// <returns></returns>
    private BigInteger GenerateNumberByDivisor(int parSize, BigInteger parDivisor)
    {
      BigInteger lowBound = BigInteger.Divide(BigInteger.Pow(2, parSize - 1), parDivisor) * parDivisor;
      if (lowBound <= BigInteger.Pow(2, parSize - 1))
      {
        lowBound += parDivisor;
      }

      BigInteger hightBound = BigInteger.Divide(BigInteger.Pow(2, parSize), parDivisor) * parDivisor;
      if (hightBound >= BigInteger.Pow(2, parSize))
      {
        hightBound -= parDivisor;
      }

      while (!CheckBeloningToInterval(lowBound, L) &&
        !CheckNumberForPrimality(lowBound + 1))
      {
        lowBound += parDivisor;
      }

      return lowBound;
    }

    /// <summary>
    /// Check number for beloning to the interval
    /// </summary>
    /// <param name="parNumber"></param>
    /// <param name="parPower"></param>
    /// <returns></returns>
    private bool CheckBeloningToInterval(BigInteger parNumber, int parPower)
    {
      return BigInteger.Pow(2, parPower) > parNumber &&
             BigInteger.Pow(2, parPower - 1) < parNumber;
    }

    private bool CheckNumbersForRelativelyPrimality(BigInteger parFirstNum, BigInteger parSecondNum)
    {
      var numbers = new List<int> { 2, 3, 4, 5, 6, 7, 8, 9 };
      var firstNumberDivisors = numbers.Where(t => parFirstNum % t == 0).ToList();

      return firstNumberDivisors.All(element => parSecondNum % element != 0);
    }

    private bool CheckNumberForPrimality(BigInteger parNumber)
    {
      var numbers = new List<int> { 2, 3, 4, 5, 6, 7, 8, 9 };
      return numbers.All(element => parNumber % element != 0);
    }

    private BigInteger Pow(BigInteger parNumber, BigInteger parExtent)
    {
      BigInteger temp = parNumber;
      for (int i = 0; i < parExtent - 1; i++)
      {
        temp *= parNumber;
      }
      return temp;
    }
  }
}
